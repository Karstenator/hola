package Restoran.Controlador;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import Restoran.Modelo.*;
import Restoran.Modelo.Persona;
import java.util.ArrayList;
public class ControladorPedidosRestoran {
    private static ControladorPedidosRestoran instance = null;
    private ArrayList<Producto> productos;
    private ArrayList<Cliente> clientes;
    private ArrayList<Pedido> pedidos;
    private ArrayList<Repartidor> repartidores;
    private ControladorPedidosRestoran(){
        productos = new ArrayList<>();
        clientes = new ArrayList<>();
        pedidos = new ArrayList<>();
        repartidores = new ArrayList<>();
    }
    public static ControladorPedidosRestoran getInstance(){
        if (instance == null){
            instance = new ControladorPedidosRestoran();
        }
        return instance;
    }
    public int addPedido(String rutCliente, int[][] datosProductos){
        return -1;
    }
    public void addCliente(String rut, String nombre, String direccion, String telefono){
        clientes.add(new Cliente(rut,nombre,direccion,telefono));
    }
    public void addRepartidor(String rut, String nombre, String domicilio, String telefono){
        repartidores.add(new Repartidor(rut,nombre,domicilio,telefono));
    }
    public void addProductoBásico(int codigo, String nombre, int precioCosto, int margenVenta){

    }
    public void addStockAProducto(int codigo, int numeroUnidades){

    }
    public void setRepartidorAPedido(String rut, int codigo){

    }
    public void setEntregaPedido(int codigo){

    }
    public String[][] listProductos(){
        return null;
    }
    public String[][] listRepartidores(){
        return null;
    }
    public String[][] listPedidosPendientes(String rutCliente){
        Cliente cliente = findCliente(rutCliente);
        Pedido[] pedidosPendientes = cliente.getPedidosPendientes();
        if (pedidosPendientes.length > 0) {
            String[][] datosPedidos = new String[pedidosPendientes.length][4];
            int i=0;
            for (Pedido pedido : pedidosPendientes) {
                datosPedidos[i][0] = String.valueOf(pedido.getNumero());
                datosPedidos[i][1] = pedido.getFecha().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL));
                datosPedidos[i][2] = Persona.getRut();
                datosPedidos[i][3] = Persona.getRut().;
                i++;
            }
            return datosPedidos;
        }
        return new String[0][0];
    }
    public String[][] listPedidosPorEntregar(String rutRepartidor){
        return null;
    }
    public String[][] listProductosMasSolicitados(int numeroPedidosMinimos){
        return null;
    }
    public String[] getDatosCliente(String rut){
        return new String[0];
    }
    public String[] getDatosProducto(int codigo){
        return null;
    }
    public String[] getDatosRepartidor(String rut){
        return null;
    }
    public void readDatosSistema(){
        
    }
    public void saveDatosSistema(){
        
    }
    private Cliente findCliente(String rut) {
        for (Cliente cliente : clientes) {
            if (Persona.getRut().equalsIgnoreCase(rut)) {
                return cliente;
            }
        }
        return null;
    }
}