package Restoran.Modelo;
import Restoran.Modelo.Persona;
import java.util.ArrayList;
public class Cliente {
    private final ArrayList<Pedido> pedidos;
    public Cliente(String rut, String nombre, String domicilio, String telefono){
        this.pedidos = new ArrayList<>();
    }
    public void addPedido(Pedido pedido){
        this.pedidos.add(pedido);
    }
    public Pedido[] getPedidosPendientes(){
        ArrayList<Pedido> pedidosPendientes = new ArrayList<>();
        for (Pedido pedido : pedidos){
            if (pedido.getEstado() == Pedido.Estado.PENDIENTE){
                pedidosPendientes.add(pedido);
            }
        }
        return pedidosPendientes.toArray(new Pedido[0]);
    }
    public int getNumeroPedidos(){
        return 0;
    }
}