package Restoran.Modelo;
public class LineaPedido {
    private int cantidad;
    private final Pedido pedido;
    private final Producto producto;
    public LineaPedido(Pedido pedido, Producto producto, int cantidad){
        this.cantidad = cantidad;
        this.pedido = pedido;
        this.producto = producto;
        producto.addLineaPedido(this);
    }
    public int getSubTotal(){
        return cantidad * producto.getPrecioVenta();
    }
    @Override
    public String toString(){
        return String.valueOf(cantidad);
    }
}