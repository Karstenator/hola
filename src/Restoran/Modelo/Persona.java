package Restoran.Modelo;
public abstract class Persona {
    private String rut;
    private String nombre;
    private String dirección;
    private String telefono;
    public Persona(String nombre, String direccion, String telefono){
        this.nombre = nombre;
        this.dirección = direccion;
        this.telefono = telefono;
    }
    public void getRut(){

    }
    public void getNombre(){

    }
    public static boolean validaRut(String rut){
        return true;
    }
    public abstract void addPedido(Pedido pedido);
    public abstract Pedido getPedidosPendientes();
    public abstract int getNumeroPedidos();
    public boolean equals(Persona other){
        return other == this;
    }
    @Override
    public String toString(){
        return(this.rut)+" ,"+(this.nombre)+" ,"+(this.dirección)+" ,"+(this.telefono);
    }
}