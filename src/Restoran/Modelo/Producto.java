package Restoran.Modelo;
import java.util.ArrayList;
import java.util.Objects;
public class Producto {
    public static Producto productos;
    private int codigo;
    private String nombre;
    private int precioCosto;
    private int margenVentas;
    private int stock;
    private final ArrayList<LineaPedido> lineasPedidos;
    public Producto(int codigo, String nombre, int costo, int margen){
        this.codigo = codigo;
        this.nombre = nombre;
        this.precioCosto = costo;
        this.margenVentas = margen;
        this.stock = 0;
        this.lineasPedidos = new ArrayList<>();
    }
    public int getCodigo(){
        return codigo;
    }
    public String getNombre(){
        return nombre;
    }
    public int getPrecioVenta(){
        return (int) (precioCosto * (1 + margenVentas/100.0));
    }
    public void addLineaPedido(LineaPedido linea){
        this.lineasPedidos.add(linea);
    }
    public int getNumeroLineasPedidos(){
        return lineasPedidos.size();
    }
    public void addStock(int numeroUnidades){
        stock += numeroUnidades;
    }
    public boolean removeStock(int numeroUnidades){
        if (stock >= numeroUnidades) {
            stock -= numeroUnidades;
            return true;
        }
        return false;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Producto producto = (Producto) o;
        return codigo == producto.codigo;
    }
    @Override
    public int hashCode() {

        return Objects.hash(codigo);
    }
    public boolean equals(Producto other){
        return other == this;
    }
    @Override
    public String toString(){
        return (this.codigo)+", "+(this.nombre)+", "+(this.precioCosto)+", "+(this.margenVentas);
    }
}