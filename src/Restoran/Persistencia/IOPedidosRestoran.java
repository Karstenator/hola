package Restoran.Persistencia;
import Restoran.Modelo.Producto;
import Restoran.Modelo.Cliente;
import Restoran.Modelo.Repartidor;
import Restoran.Modelo.Pedido;
public class IOPedidosRestoran {
    private static IOPedidosRestoran instance = null;
    private IOPedidosRestoran(){
        
    }
    public IOPedidosRestoran getInstance(){
        if (instance == null){
            instance = new IOPedidosRestoran();
        }
        return instance;
    }
    public Producto[] readProductosSinPedidos(){
        return null;
    }
    public Cliente[] readClientesSinPedidos(){
        return null;
    }
    public Repartidor[] readRepartidoresSinPedidos(){
        return null;
    }
    public Pedido[] readPedidos(){
        return null;
    }
    public void saveProductosYPersonasSinPedidos(Producto[] producto, Cliente[] cliente, Repartidor[] repartidor){
        
    }
    public void savePedidos(Pedido[] pedidos){
        
    }
    public void saveComprobantePedido(Pedido[] pedido){
        
    }
}