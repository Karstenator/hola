package Restoran.Vista;
import Restoran.Controlador.ControladorPedidosRestoran;
import javax.swing.*;
import java.util.Scanner;
public class UIPedidosRestoran {
    Scanner teclado = new Scanner(System.in);
    private static UIPedidosRestoran instance = null;

    private UIPedidosRestoran() {
        teclado.useDelimiter("\t|\r\n|[\n\r\u2028\u2029\u0085]");
    }

    public static UIPedidosRestoran getInstance() {
        if (instance == null) {
            instance = new UIPedidosRestoran();
        }
        return instance;
    }

    public void menuPrincipal() {
        creaDatosDePrueba();
        int opcion;
        do {
            System.out.println("..........................................................");
            System.out.println("...........:::SISTEMA DE PEDIDOS DE RESTORÁN:::...........");
            System.out.println();
            System.out.println("...:::MENÚ PRINCIPAL:::...");
            System.out.println(" 1. Crear cliente/repartidor");
            System.out.println(" 2. Crear producto");
            System.out.println(" 3. Ingresar pedido");
            System.out.println(" 4. Ingresar entrega pedido");
            System.out.println(" 5. Listar productos");
            System.out.println(" 6. Listar repartidores");
            System.out.println(" 7. Listar pedidos pendientes");
            System.out.println(" 8. Listar pedidos por entregar");
            System.out.println(" 9. Listar productos más solicitados");
            System.out.println("10. Salir");
            System.out.println();
            System.out.println("\tIngrese opción:");
            opcion = teclado.nextInt();
            switch (opcion) {
                case 1:
                    creaClienteRepartidor();
                    System.out.println();
                    break;
                case 2:
                    creaProducto();
                    System.out.println();
                    break;
                case 3:
                    ingresaPedidoCliente();
                    System.out.println();
                    break;
                case 4:
                    ingresaEntregaPedido();
                    System.out.println();
                    break;
                case 5:
                    listaProductos();
                    System.out.println();
                    break;
                case 6:
                    listaRepartidores();
                    System.out.println();
                    break;
                case 7:
                    listaPedidosPendientes();
                    System.out.println();
                    break;
                case 8:
                    listaPedidosPorEntregar();
                    System.out.println();
                    break;
                case 9:
                    listaProductosMasSolicitados();
                    System.out.println();
                    break;
                case 10:
                    break;
                default:
                    System.out.println("Error, opción no válida. Inténtelo de nuevo.");
                    System.out.println();
            }
        } while (opcion != 10);
        System.out.println("Gracias por operar este sistema");
    }
    private void creaClienteRepartidor() {
        System.out.println("\nCREANDO CLIENTE O REPARTIDOR...");
        String rut;
        String nombre;
        String direccion;
        String telefono;
        int opcion;
        System.out.println("\nMenu de opciones");
        System.out.println("1. Crear cliente");
        System.out.println("2. Crear repartidor");
        do {
            System.out.print("\tIngrese opcion: ");
            opcion = teclado.nextInt();
            if (opcion != 1 && opcion != 2) {
                System.out.println("** Opcion erronea **");
            }
        } while (opcion != 1 && opcion != 2);
        if (opcion == 1){
            Scanner teclado = new Scanner(System.in);
            System.out.println("\nCREANDO CLIENTE");
            System.out.print("Rut: ");
            rut = teclado.nextLine();
            System.out.print("Nombre: ");
            nombre = teclado.nextLine();
            System.out.print("Direccion: ");
            direccion = teclado.nextLine();
            System.out.print("Telefono: ");
            telefono = teclado.nextLine();
            ControladorPedidosRestoran.getInstance().addCliente(rut, nombre, direccion, telefono);
            System.out.println("Su cliente ha sido creado con exito");
        }else{
            System.out.println("\nCREANDO REPARTIDOR");
            System.out.println("Rut: ");
            rut = teclado.nextLine();
            System.out.print("Nombre: ");
            nombre = teclado.nextLine();
            System.out.print("Direccion: ");
            direccion = teclado.nextLine();
            System.out.print("Telefono: ");
            telefono = teclado.nextLine();
            ControladorPedidosRestoran.getInstance().addRepartidor(rut, nombre, direccion, telefono);
            System.out.println("Su repartidor ha sido creado con exito");
        }
    }
    private void creaProducto() {
        
    }
    private void ingresaPedidoCliente() {
        
    }
    private void ingresaEntregaPedido() {
        
    }
    private void listaProductos() {
        
    }
    private void listaRepartidores() {
        
    }
    private void listaPedidosPendientes() {
        String rutCLiente;
        System.out.println("Ingrese rut del cliente en cuestión:");
        String rutCliente = teclado.nextLine();
        ControladorPedidosRestoran.getInstance().listPedidosPendientes(rutCliente);
    }

    private void listaPedidosPorEntregar() {
        
    }
    private void listaProductosMasSolicitados() {
        
    }
    private void creaDatosDePrueba() {
        
    }
}